function [ sample ] = GetSamplePointsSLIC( suppix, ucm)
%GETSAMPLEPOINTS Get sample points from an image using SLIC
% tname: slic name
% sp_dist: sample distance
pts=seg2bmap(suppix);
[row, col]=find(pts);
sample=[row col];
[H, W]=size(suppix);
t=3;
for i=1:length(sample)
    y=sample(i,1);
    x=sample(i,2);
    y1=max(y-t,1);
    y2=min(y+t,H);
    x1=max(x-t,1);
    x2=min(x+t,W);
    
    subpatch=ucm(y1:y2,x1:x2);
    if(sum(sum(subpatch))==0)
        sample(i,1)=0;
        sample(i,2)=0;
    end
end

sample(~any(sample,2),:)=[];

end
