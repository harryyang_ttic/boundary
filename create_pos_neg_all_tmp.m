subwinSize=35;
class='aeroplane';
foldername=fullfile('/share/data/vision-greg/harry',class);
logname='log.txt';

%% init
fid2=fopen(logname,'w');
if ~exist(foldername)
    mkdir(foldername);
end

addpath('/share/project/shapes/harry/datahandling');
ds=mapDataSets('voc12','fg-all',class);

%% create splitted hdf5 files with
parfor i=1:length(ds.imnames)
    mat=load(fullfile(ds.spdirs.slic,[ds.imnames{i},'_slic_k600_m15.mat']));
    im=mat.im;
    [H, W,~]=size(im);

    gt=ds.loadGT(i);
    gt=gt.groundTruth{1}.Segmentation;
    gt(gt==255)=0;
    gt=uint8(gt);
    gt(gt~=2)=0;
    gt(gt==1)=1;

    if exist(fullfile('/share/data/vision-greg/Pascal/VOCdevkit/VOC2012/ucm2',[ds.imnames{i} '.mat']))
        continue;
    end
    
    d=ds.loadUCM(i);
    % d=d(1:2:end,1:2:end);
    [h1, w1]=size(d);
    fprintf('%d %d %d %d %d\n',i,H,W,h1,w1);
    d=d>0.05;

    im2=rgb2gray(im);
    [ sample, suppix ]=GetSamplePointsUCM( im2, d );    
    % sample=GetSamplePointsSLIC(suppix,d);
    tic
    pos_num=SamplePosPoints(im, suppix, gt, sample, subwinSize, foldername, ds.imnames{i});
    neg_num=SampleNegPoints(im, suppix, gt, sample, subwinSize, foldername, ds.imnames{i});
    toc
    fprintf('%s pos: %d neg: %d\n',ds.imnames{i},pos_num,neg_num);
    %fprintf(fid2,'%s pos: %d neg: %d\n',ds.imnames{i},pos_num,neg_num);
end

%fclose(fid);
%fclose(fid2);




