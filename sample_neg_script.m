clear;

tname='2007_000033'

mat=load(strcat('data/',tname,'_slic_k600_m15.mat'));
im=mat.im;
[H, W,~]=size(im);
suppix=mat.suppix;

num_of_sup=max(max(suppix));

gt=load(strcat('data/',tname,'.mat'));
gt=gt.groundTruth{1}.Segmentation;
gt(gt==255)=0;
gt=uint8(gt);

psize=35;
im2=rgb2gray(im);

%% Get sample points
mask=zeros(H,W);
sp_dist=0;
i=1;
for h=1:H
    for w=1:W
        if is_boundary(suppix,h,w) 
           sample(i,:)=[h,w];
           h1=h;
           h2=min(h+sp_dist,H);
           w1=w;
           w2=min(w+sp_dist,W);
           mask(h1:h2,w1:w2)=1; 
           i=i+1;
        end  
    end
end

%% sample and save negative training data
half_size=int32(psize/2);
patch_id=0;
%mask=zeros(H,W);

center_thresh=5;
negOvlpThresh = 0.5;

if exist(sprintf('res/%s/neg/',tname))==0
    mkdir(sprintf('res/%s/neg/',tname));
end   

mask=zeros(H,W);
for i=1:length(sample)
    h=sample(i,1)-half_size;
    w=sample(i,2)-half_size;
    if h<1 || w<1 || h+psize-1 > H || w+psize-1 > W || mask(sample(i,1),sample(i,2))==1
        continue;
    end
    
    testPatchImg = im(h:h+psize-1,w:w+psize-1,:);
    cur_sup=suppix(h:h+psize-1,w:w+psize-1);
    
    cur_sup_cluster = cluster_patch(testPatchImg, cur_sup,10);
    
    cluster_id=unique(cur_sup_cluster);
    cluster_num=length(cluster_id);
    
    cur_gt=gt(h:h+psize-1,w:w+psize-1);
    cur_gt(cur_gt~=2)=0;
    cur_gt(cur_gt==2)=1;
    
    flag1=is_center_on_boundary(cur_sup_cluster,half_size,1);
    if flag1==false
        continue;
    end
    
    inner_patch_id=0;
    for j=1:power(2,cluster_num)
        str=dec2bin(j-1,cluster_num);
        testPatchMask=zeros(psize,psize);
        for k=1:cluster_num
           testPatchMask(cur_sup_cluster==k)=str2num(str(k));
        end
        flag1=is_center_on_boundary(testPatchMask,half_size,1);
        
        if flag1==false
            continue;
        end
        flag2=is_connected(testPatchMask);
        if flag2==false
            continue;
        end
        
        ovlpGT=fbInitUni(testPatchMask,double(cur_gt));
        if ovlpGT>negOvlpThresh
            if is_center_on_boundary(testPatchMask,half_size,5)
                continue;
            end
        end
        
        inner_patch_id=inner_patch_id+1;
        if mod(inner_patch_id,4)~=1
            continue;
        end
        
        patch_res=im(h:h+psize-1, w:w+psize-1,:);
        for k=1:psize
            for l=1:psize
                if is_boundary(testPatchMask,k,l)
                    patch_res(k,l,1)=255;
                    patch_res(k,l,2)=0;
                    patch_res(k,l,3)=0;
                end
            end
        end
        
        
        fname=sprintf('res/%s/neg/2007_000480_%d.ppm',tname,patch_id);
        imwrite(patch_res,fname);
        patch_id=patch_id+1;
    end    
    
   h1=max(1,sample(i,1)-3);
   h2=min(sample(i,1)+3,H);
   w1=max(1,sample(i,2)-3);
   w2=min(sample(i,2)+3,W);
   mask(h1:h2,w1:w2)=1;
end
   
   