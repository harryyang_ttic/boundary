function [patch_id ] = SampleNegPoints( im, suppix, gt, sample, psize, save_folder, image_name)
%SAMPLENEGPOINTS Summary of this function goes here
%   Detailed explanation goes here
half_size=int32(psize/2);
[H, W,~]=size(im);
mask=zeros(H,W);
perm=randperm(length(sample));

negOvlpThresh = 0.5;
patch_id=0;

mask_res=cell(0);
pos_res=cell(0);

for i=1:length(sample)
    h=sample(perm(i),1)-half_size;
    w=sample(perm(i),2)-half_size;
    if h<1 || w<1 || h+psize-1 > H || w+psize-1 > W || mask(sample(perm(i),1),sample(perm(i),2))==1
        continue;
    end
    
    cur_sup=suppix(h:h+psize-1,w:w+psize-1);
    cur_gt=gt(h:h+psize-1,w:w+psize-1);
    
    [res,cur_sup,cluster_num, ~]=RandomPartition(cur_sup,psize,4,50);
    [res_num,~]=size(res);
    if res_num<1
        continue;
    end
    
    perm2=randperm(res_num);
    for j=1:res_num
        str=res(perm2(j),:);
        tmp_patch=zeros(psize,psize);
        for k=1:cluster_num
           tmp_patch(cur_sup==k)=str(k);
        end

        ovlpGT=fbInitUni(tmp_patch,double(cur_gt));
        if ovlpGT>negOvlpThresh
           continue;
        end
        mask_res(end+1)={tmp_patch};
        pos_res(end+1)={[h w]};
    
        patch_id=patch_id+1;
        break;
    end    
    
   h1=max(1,sample(perm(i),1)-2);
   h2=min(sample(perm(i),1)+2,H);
   w1=max(1,sample(perm(i),2)-2);
   w2=min(sample(perm(i),2)+2,W);
   mask(h1:h2,w1:w2)=1;
end
res_name=sprintf('%s/%s_neg.mat',save_folder,image_name);
save(res_name,'mask_res','pos_res');    
end

