psize=35;
class='aeroplane';
foldername=class;
logname='log.txt';

%% init
fid2=fopen(logname,'w');
if ~exist(foldername)
    mkdir(foldername);
end
trainFileName = [foldername '/' class];

%% create splitted hdf5 files with
file_id=1;
patch_id=1;

i=1;
mat=load('data/2007_000033_slic_k600_m15.mat');
im=mat.im;
[H, W,~]=size(im);
suppix=mat.suppix;

gt=load('data/2007_000033.mat');
gt=gt.groundTruth{1}.Segmentation;
gt(gt==255)=0;
gt=uint8(gt);
gt(gt~=2)=0;
gt(gt==1)=1;

ucm2=load('data/2007_000033_ucm.mat');

c=struct2cell(ucm2);
d=c{1};
d=d(1:2:end,1:2:end);
d=d>0.05;

im2=rgb2gray(im);
[ sample, suppix ]=GetSamplePointsUCM( im2, d );
%sample=GetSamplePointsSLIC(suppix,d);
tic
pos_num=SamplePosPoints(im, suppix, gt, sample, psize, foldername, '2007_000033');
neg_num=SampleNegPoints(im, suppix, gt, sample, psize, foldername, '2007_000033');
toc

fprintf(fid2,'%s pos: %d neg: %d\n','2007_000033',pos_num,neg_num);

fclose(fid2);




