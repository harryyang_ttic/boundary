function [ res, cur_pat, cluster_num, iter_count ] = RandomPartition( cur_sup,psize, t1, t2 )
%RANDOMPARTITION Summary of this function goes here
%   Detailed explanation goes here
res=[];
iter_count=0;

unique_id=unique(cur_sup);
cluster_num=length(unique_id);

[cur_sup]=FindDisconnected(cur_sup, cluster_num, unique_id);
[cur_sup, cluster_num]=Relabel(cur_sup);
adj=FindAdjMatrix(cur_sup, cluster_num);
[cur_sup]=RemoveSmallAreas(cur_sup, cluster_num,adj, 50);
[cur_sup, cluster_num]=Relabel(cur_sup);
adj=FindAdjMatrix(cur_sup, cluster_num);
cur_pat=cur_sup;

center_id=FindCenterId(cur_pat,psize,3);
if length(center_id)==1
    return;
end

is_inside=FindInteriorId(cur_pat,cluster_num);

count=1;
iter_count=0;
res=zeros(t1,cluster_num);

while count<t1 && iter_count<t2
    mask=zeros(cluster_num,1);
    rc=randi(length(center_id));
    mask(center_id(rc))=1;
    nb=adj(center_id(rc),:);
    
    %% random grow
    while nnz(mask(center_id))<mod(iter_count,length(center_id)-1)+1
        nb2=find(nb);
        t=nb2(randi(length(nb2)));
        mask(t)=1;
        nb=nb | adj(t,:);
        nb=nb & ~mask';
    end
 
    if length(unique(mask(~is_inside)))==1
       iter_count=iter_count+1;
       continue;
    end
    
    %{
    allow the background to be disconnected?
    flag=CheckConnected(adj,mask);
    if ~flag
        iter_count=iter_count+1;
        continue;
    end
    %}
   
    res(count,:)=mask;
    res(count+1,:)=~mask;
   
    count=count+2;
    iter_count=iter_count+1;  
end

%fprintf('%d\n',iter_count);

res=unique(res,'rows');
res(all(~res,2),:)=[];

end

function [cur_sup]=FindDisconnected(cur_sup, cluster_num, unique_id)
    k=0;
    max_id=max(unique_id);
    for i=1:cluster_num
        tmp_pat=(cur_sup==unique_id(i));
        tmp_l=bwlabel(tmp_pat,4);
        for j=2:max(max(tmp_l)) 
            k=k+1;
            cur_sup(tmp_l==j)=max_id+k;
        end
    end
end

function [adj]=FindAdjMatrix(cur_pat, cluster_num)
    adj=zeros(cluster_num,cluster_num);
    [H,W]=size(cur_pat);
    for h=1:H
        for w=1:W
            label=cur_pat(h,w);
            if h-1>0 && cur_pat(h-1,w)~=label
                adj(label,cur_pat(h-1,w))=adj(label,cur_pat(h-1,w))+1;
            end
             if h+1<H && cur_pat(h+1,w)~=label
                adj(label,cur_pat(h+1,w))=adj(label,cur_pat(h+1,w))+1;
             end
             if w-1>0 && cur_pat(h,w-1)~=label
                adj(label,cur_pat(h,w-1))=adj(label,cur_pat(h,w-1))+1;
             end
             if w+1<W && cur_pat(h,w+1)~=label
                adj(label,cur_pat(h,w+1))=adj(label,cur_pat(h,w+1))+1;  
             end
        end
    end
    adj=adj>0;
    adj=double(adj);
end

function [cur_pat]=RemoveSmallAreas(cur_pat, cluster_num, adj, t)
    for i=1:cluster_num
        pat_num=nnz(cur_pat==i);
        label=find(adj(i,:));
        if pat_num<t 
            cur_pat(cur_pat==i)=label(1);
        end
    end
end

function [cur_pat, cluster_num]=Relabel(cur_sup)
    unique_id=unique(cur_sup);
    cluster_num=length(unique_id);
    cur_pat=zeros(size(cur_sup));
    for i=1:cluster_num
        cur_pat(cur_sup==unique_id(i))=i;
    end
end

function [center_id]=FindCenterId(cur_pat,psize,t)
    half_size=int32(psize/2);
    center_patch=cur_pat(half_size-t:half_size+t,half_size-t:half_size+t);
    center_id=unique(center_patch);
   
end

function [is_inside]=FindInteriorId(cur_pat,cluster_num)
    [H,W]=size(cur_pat);
    is_inside=zeros(cluster_num,1);
    for h=1:H
        for w=1:W
            if h>1 && h<H && w>1 && w<W
                continue;
            end
            is_inside(cur_pat(h,w))=is_inside(cur_pat(h,w))+1;
        end
    end
    is_inside=is_inside<2;
    is_inside=double(is_inside);
end

function [flag]=CheckConnected(adj,mask)
    B=adj(~mask,~mask);
    B=B+eye(size(B));
    [nb,~]=size(B);
    C=B;
    flag=false;
    for i=1:nb
        D=C>0;
        if sum((all(D==1,2)))>0
            flag=true;
            return;
        end
        C=C*B;
    end
end