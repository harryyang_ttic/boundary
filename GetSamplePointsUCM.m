function [ sample, suppix ] = GetSamplePointsUCM( im2, ucm )
%GETSAMPLEPOINTSUCM Summary of this function goes here
%   Detailed explanation goes here

%label regions
[H,W]=size(im2);
CC=bwlabel(1-ucm);

for h=1:H
    for w=1:W
        if CC(h,w)==0
            pix=im2(h,w);
            k=1;
            while true
                tmpcc=CC(max(h-k,1):min(H,h+k),max(1,w-k):min(W,w+k));
                if nnz(tmpcc)==0
                    k=k+1;
                    continue;
                end   
                break;
            end
            tmp=im2(max(h-k,1):min(H,h+k),max(1,w-k):min(W,w+k));    
            tmp=abs(int32(tmp)-int32(pix));
            tmp(tmpcc==0)=1000;
            [h2,y2]=find(tmp==min(min(tmp)));
            CC(h,w)=tmpcc(h2(1),y2(1));
        end
    end
end

[I,J]=ind2sub(size(ucm),find(ucm));
sample=[I,J];
suppix=CC;

end

