subwinSize=35;
class='aeroplane';
foldername=['/share/data/vision-greg/harry/' class];
logname='log.txt';

%% init
fid2=fopen(logname,'w');
if ~exist(foldername)
    mkdir(foldername);
end
trainFileName = [foldername '/' class];

addpath('/share/project/shapes/harry/datahandling');
ds=mapDataSets('voc12','fg-all',class);

%% create splitted hdf5 files with
parfor i=1:length(ds.imnames)
    mat=load(fullfile(ds.spdirs.slic,[ds.imnames{i},'_slic_k600_m15.mat']));
    im=mat.im;
    [H, W,~]=size(im);

    gt=ds.loadGT(i);
    gt=gt.groundTruth{1}.Segmentation;
    gt(gt==255)=0;
    gt=uint8(gt);
    gt(gt~=2)=0;
    gt(gt==2)=1;
    
    d=ds.loadUCM(i);
    d=d>0.1;

    im2=rgb2gray(im);
    [ sample, suppix ]=GetSamplePointsUCM( im2, d );    
    %sample=GetSamplePointsSLIC(suppix,d);
    
    gt2=zeros(H,W);
    suppix_id=unique(suppix);
    for j=1:length(suppix_id)
        fg=sum(sum(gt(suppix==suppix_id(j))));
        bg=nnz(suppix==suppix_id(j));
        if fg>bg*0.5
            gt2(suppix==suppix_id(j))=1;
        elseif fg<bg*0.5
            gt2(suppix==suppix_id(j))=0;
        end
    end
    gt=gt2;
    tic
    pos_num=SamplePosPoints(im, suppix, gt, sample, subwinSize, foldername, ds.imnames{i});
    neg_num=SampleNegPoints(im, suppix, gt, sample, subwinSize, foldername, ds.imnames{i});
    toc
    fprintf('%s pos: %d neg: %d\n',ds.imnames{i},pos_num,neg_num);
    %fprintf(fid2,'%s pos: %d neg: %d\n',ds.imnames{i},pos_num,neg_num);
end

%fclose(fid);
%fclose(fid2);




