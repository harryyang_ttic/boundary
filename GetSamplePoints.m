function [ sample ] = GetSamplePoints( suppix, sp_dist )
%GETSAMPLEPOINTS Get sample points from an image using SLIC
% tname: slic name
% sp_dist: sample distance
i=1;
[H,W]=size(suppix);
mask=zeros(H,W);
for h=1:H
    for w=1:W
        if is_boundary(suppix,h,w) && mask(h,w)==0
           sample(i,:)=[h,w];
           h1=h;
           h2=min(h+sp_dist,H);
           w1=w;
           w2=min(w+sp_dist,W);
           mask(h1:h2,w1:w2)=1; 
           i=i+1;
        end  
    end
end

end

