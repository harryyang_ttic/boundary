function [ flag ] = is_boundary( suppix, h, w )
%IS_BOUNDARY Summary of this function goes here
%   Detailed explanation goes here
flag=false;
[H,W]=size(suppix);
id=suppix(h,w);
for i=h-1:h+1
    for j=w-1:w+1
        if(i<1 || i>H || j<1 || j>W)
            continue;
        end
        cur_id=suppix(i,j);
        if cur_id ~= id
            flag=true;
        end
    end
end

end

