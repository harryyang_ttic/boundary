function [patch_id] = SamplePosPoints( im, suppix, gt, sample, psize, save_folder, image_name)
%SAMPLEPOSPOINTS Summary of this function goes here
%   Detailed explanation goes here
%% init
half_size=int32(psize/2);
[H, W,~]=size(im);
patch_id=0;
mask=zeros(H,W);
perm=randperm(length(sample));

mask_res=cell(0);
pos_res=cell(0);

for i=1:length(sample)
    h=sample(perm(i),1)-half_size;
    w=sample(perm(i),2)-half_size;
    if h<1 || w<1 || h+psize-1 > H || w+psize-1 > W || mask(h,w)==1
        continue;
    end
    
    %test if it's on boundary
    cur_gt=gt(h:h+psize-1,w:w+psize-1);
    
    flag=is_center_on_boundary(cur_gt,half_size,1);
    if flag==false
        continue;
    end
     
    tmp_patch=cur_gt;
    
    % multiple connected components
    con=bwconncomp(tmp_patch);
    L=labelmatrix(con);
    for ci=1:con.NumObjects
        tmp_patch2=zeros(psize,psize);
        tmp_patch2(L==ci)=1;
        flag=is_center_on_boundary(tmp_patch2,half_size,1);
        if flag==false
            tmp_patch(L==ci)=0;
        end
    end
    
    mask_res(end+1)={tmp_patch};
    pos_res(end+1)={[h w]};
    
    h1=max(1,h-1);
    h2=min(H,h+1);
    w1=max(1,w-1);
    w2=min(W,w+1);
    mask(h1:h2,w1:w2)=1;
    
    patch_id=patch_id+1;
end

res_name=sprintf('%s/%s_pos.mat',save_folder,image_name);
save(res_name,'mask_res','pos_res');

end

