function [flag] = is_connected(patch)
    flag=true;
    t=bwconncomp(patch);
    if t.NumObjects>1
        flag=false;
    end
    t2=bwconncomp(1-patch);
    if t2.NumObjects>1
        flag=false;
    end  
    
    if flag==false
        return;
    end
    
    % test if its contained in interior
    [H, W]=size(patch);
    label=patch(1,1);
    flag2=true;
    for h=1:H
        if patch(h,1)~=label || patch(h,W)~=label
            flag2=false;
        end
    end
    for w=1:W
        if patch(1,w)~=label || patch(H,w)~=label
            flag2=false;
        end
    end
    if flag2==false
        flag=true;
    else
        flag=false;
    end
end
