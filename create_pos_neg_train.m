tname='2007_000033';

mat=load(strcat('data/',tname,'_slic_k600_m15.mat'));
im=mat.im;
[H, W,~]=size(im);
suppix=mat.suppix;

gt=load(strcat('data/',tname,'.mat'));
gt=gt.groundTruth{1}.Segmentation;
gt(gt==255)=0;
gt=uint8(gt);

subwinSize=35;

trainPosDatasetFileName = 'train_pos.h5' ;

if exist(trainPosDatasetFileName,'file')
   delete(trainPosDatasetFileName);
end

if exist(trainNegDatasetFileName,'file');
    delete(trainNegDatasetFileName);
end

if ~exist(trainPosDatasetFileName,'file')
    h5create(trainPosDatasetFileName,'/data',[subwinSize subwinSize 6 Inf],'Datatype','single','ChunkSize',[subwinSize subwinSize 6 1]);
    h5create(trainPosDatasetFileName,'/label',Inf,'Datatype','single','ChunkSize',1);
    h5create(trainPosDatasetFileName,'/sample_weight',Inf,'Datatype','single','ChunkSize',1);
end;
   
trainNegDatasetFileName = 'train_neg.h5' ;
if ~exist(trainNegDatasetFileName,'file')
    h5create(trainNegDatasetFileName,'/data',[subwinSize subwinSize 6 Inf],'Datatype','single','ChunkSize',[subwinSize subwinSize 6 1]);
    h5create(trainNegDatasetFileName,'/label',Inf,'Datatype','single','ChunkSize',1);
    h5create(trainNegDatasetFileName,'/sample_weight',Inf,'Datatype','single','ChunkSize',1);
end;



sample=GetSamplePoints(suppix, 0);

%patch_id=SamplePosPoints(im, suppix, gt, sample, subwinSize, trainPosDatasetFileName,1);
patch_id=SampleNegPoints(im, suppix, gt, sample, subwinSize, trainNegDatasetFileName,1);


