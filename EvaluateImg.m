mat=load(strcat('data/2007_000033_slic_k600_m15.mat'));
im=mat.im;
[H, W,~]=size(im);
im2=im2double(im);
suppix=mat.suppix;

sample=GetSamplePoints(suppix,0);

sample_num=length(sample);

out_all=zeros(0,1);

caffePath = '../../../caffe-weighted-samples/';
matcaffePath = [caffePath 'matlab/caffe']; addpath(matcaffePath);
model_def_file = '../../model/singlenet/test_v0.3.prototxt';
model_file='../../model/singlenet/data/train_iter_342084.caffemodel';
modelStr = textread(model_def_file,'%s','delimiter','\n');

matcaffe_init(1,model_def_file,model_file);

for i=1:40:sample_num
    out_prob=EvaluateBatchData( im, im2,suppix, sample, 35, i, 40);
    out_all=[out_all;out_prob];
    fprintf('%d\n',i);
end

Visualize(sample, out_all,H,W);