function [ res ] = cluster_patch( patch, sup, cutoff )
%CLUSTER_PATCH Summary of this function goes here
%   Detailed explanation goes here
[~, ~, noc]=size(patch);
if noc>1
    patch=rgb2gray(patch);
end
id=unique(sup);
colors=zeros(length(id),1);

for i=1:length(id)
    color=mean2(patch(sup==id(i)));
    colors(i)=color;
end

cluster_id=clusterdata(colors,'criterion','distance','cutoff',cutoff);

res=zeros(size(sup));
for i=1:length(id)
    res(sup==id(i))=cluster_id(i);  
end

end

