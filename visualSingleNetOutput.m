function [] = visualSingleNetOutput(nsampTest, winSizeList,varargin)
% load in the digit database (only needs to be done once per session)
%%
getAllTestData = 0;
countNegSample = 0;
regetPosTestData = 0;
regetNegTestData = 1;
%% Matcaffe path
targetClassName = 'aeroplane';
codePath = '/share/project/shapes/codes/'; addpath(codePath);
caffePath = '/share/project/shapes/caffe-weighted-samples/';
matcaffePath = [caffePath 'matlab/caffe']; addpath(matcaffePath);
model_def_file = [caffePath 'examples/singleNet/test_v0.3.prototxt'];
model_file = [caffePath 'examples/singleNet/data/train_iter_318492.caffemodel'];
modelStr = textread(model_def_file,'%s','delimiter','\n');
saveResFolder = '/share/project/shapes/singleNet/data/';
saveVisClsPath = ['/share/project/shapes/singleNet/plots/' targetClassName '/']; if ~isdir(saveVisClsPath) mkdir(saveVisClsPath); end;
if ~exist('nsampPos','var')
    nsampTest = 40; end;
batchSize = strValSearch(modelStr, 'input_dim: %d');
batchDimLineNums = strSearch(modelStr,'input_dim:');
batchSize = batchSize(1);
if batchSize ~= nsampTest
    modelStr{batchDimLineNums(1)} = sprintf('input_dim: %d',nsampTest);
    fid = fopen(model_def_file,'w');
    fprintf(fid,'%s\n',modelStr{:})
    fclose(fid);
    modelStr = textread(model_def_file,'%s','delimiter','\n');
    batchSize = strValSearch(modelStr, 'input_dim: %d');
    batchSize = batchSize(1);
end;
use_gpu = 1;
%%
fs = filesep;
netDataPath = '/scratch/stephenchen/shapes/singleNet/'; %'/share/project/shapes/singleNet/'; % [pwd fs];
sampleDataPath = '/share/project/shapes/singleNet/samples/';
if ~exist('winSizeList','var')
    %     winSizeList = [3 5 7 9 11 13 15 17 19 21 23 25 27];
    winSizeList = [35];%[7 13 19];
    % winSizeList = [3 7 11 15 19 23 27];
end;

savePosTestPreFileName = ['SingleNetTestPos_' targetClassName '_' num2str(20) '.mat'];%['VOCShapeTestSet_' targetClassName '_' num2str(nsampPos) '.mat'];
% saveNegTestFileName = ['SingleNetTestNeg_' targetClassName '_' num2str(nsampPos) '.mat'];
realSegPath = '/share/project/shapes/data/segment_results/voc12berkeley/';
codePath = '/share/project/shapes/codes/'; addpath(codePath);
[vocPath, imgPath, gtPath, imgSetPath] = VOCDataPaths();
posNegPatchPath = [netDataPath 'pos_neg_patches_DB/']; if ~isdir(posNegPatchPath) mkdir(posNegPatchPath); end;
saveTestPrefix = 'testHDF';
minMaskPixThrsh = 50; % min size of mask should be 100

%%
% dsInfo = mapDataSets('voc12','fg-all',targetClassName);
addpath('/share/project/shapes/libs/shape_context/');
addpath('/share/project/shapes/vocdevkit/VOCcode');

VOCinit;
%% Image sets
dsInfo = mapDataSets('voc12','val','inst');
valList = dsInfo.imnames;
testImgNum = length(valList);
valImgNum = length(valList);

classNameList = VOCopts.classes;
targetClassLabel = find(strcmp(classNameList,targetClassName));
close all;


saveScoreFolder = '../data/cache_voc2012/';
%load([netDataPath saveNegTestFileName],'negData','negInstMasks','negInstImages','negInstGTMasks','maskW','maskH');

if ~exist('beginNum','var')
    beginNum = 1; end;
if ~exist('endNum','var')
    endNum = 1; end;
if ~exist('distortNum','var')
    distortNum = 10; % generate distorted query image
end;

%%%Define flags and parameters:
%%
winSizeNum = length(winSizeList);
tau = 0.7
%%
% testPosInstNum = length(posInstMasks);
queryNum = endNum - beginNum + 1;% length(testSet);
pixValThsh = 0.5;
r=1; % annealing rate
w=4;
sf=1;

%% Parse Vararin
for pair = reshape(varargin,2,[])
    tmp = pair{2};
    eval([pair{1} ' = tmp']);
    clear tmp;
end; %pair
checkDir(saveScoreFolder);
%%  Precompute Testing Images Patches

%% Query Image
%% A (undistorted) query image
% topMatch_ovlpgt_scores = zeros(1,winSizeNum);
% % topMatchWins_ovlpScores = zeros(distortNum, winSizeNum, queryNum);
% % topMatchWins_Idx = zeros(distortNum, winSizeNum, queryNum);
% allData = struct('queryImage',[],'distortions',[]);
% dist_ovlp_gt = cell(1,queryNum);
dx = ceil(max(winSizeList) /2);
dy = dx;
padr = ceil(max(winSizeList) /2);
%% init caffe network (spews logging info)
if exist('use_gpu', 'var')
    matcaffe_init(use_gpu,model_def_file,model_file);
else
    matcaffe_init(0,model_def_file,model_file);
end
%% get windows around sample points
accuracyHard = [];
accuracyAll = [];
allOutLabels = [];
allGTLabels = [];

subwinSize = 35;
subwinSize = winSizeList(ww); % sxs window around sample points
%     hdfPosPath = sprintf('%spos_batch_%dx%d/',hdfPath,subwinSize,subwinSize);if ~isdir(hdfPosPath) mkdir(hdfPosPath); end;
%     hdfNegPath = sprintf('%sneg_batch_%dx%d/',hdfPath,subwinSize,subwinSize);if ~isdir(hdfNegPath) mkdir(hdfNegPath); end;
disp(['---- Window Size ' num2str(subwinSize)]);
winR = floor(subwinSize/2);
%% Count Negative patche number
negCountFileName = [sampleDataPath 'negTestCount.mat'];
predClassMaskNum = 0;
curPatchNum = 0;
load([sampleDataPath savePosTestPreFileName],'maskW','maskH'); % 'nsampPos',
meanMW = round(mean(maskW)); meanMH = round(mean(maskH));
refMaskSize = [meanMH meanMW];
for mm = 1 : valImgNum % randperm(valImgNum) %1 : valImgNum
    imgFullName = [dsInfo.imdir valList{mm} '.' dsInfo.extension];
    [imgDir imgName imgFormat] = fileparts(imgFullName);
    saveVisImgFolder = [saveVisClsPath imgName '/']; 
    orgImage = im2double(imread(imgFullName));
    imgH = size(orgImage,1); imgW = size(orgImage,2);
    switch dsInfo.dataset
        case 'sbd'
            load([sbdInstPath imgName '.mat'],'GTinst');
            load([sbdClsPath imgName '.mat'],'GTcls');
            Sclass = GTcls.Segmentation;
            Sobj = GTinst.Segmentation;
            objInstIdx = unique(Sobj);
            objInstIdx = setdiff(objInstIdx, 0);%
            objInstIdx = setdiff(objInstIdx, 255);
            imgClassIdxList = GTinst.Categories;
            if ~ismember(targetClassLabel,imgClassIdxList) % not the target class
                disp(['Image has no ' targetClassName ', continue!']);
                continue;
            end;
        case 'voc12'
            rec=PASreadrecord(sprintf(VOCopts.annopath,imgName));
            imgClassList = {rec.objects.class};
            objInstIdx = find(strcmp(imgClassList,targetClassName));
            if length(objInstIdx)
                [Sobj,CMobj]=imread(sprintf(VOCopts.seg.instimgpath,imgName));
                [Sclass,CMclass]=imread(sprintf(VOCopts.seg.clsimgpath,imgName));
            else
                disp(['Image has no ' targetClassName ', continue!']);
                continue;
            end;
    end;
    imgRealSegFolder = [realSegPath imgName fs];
    imgRealSegList = dir([imgRealSegFolder '*.png']);
    targetClassGTMask = double(Sclass == targetClassLabel);
    for rr = 1 : length(imgRealSegList) % randperm(length(imgRealSegList)) % 1 : length(imgRealSegList)
        fprintf('%s Starts\n',[imgRealSegList(rr).name]);
        [realSegImg,CMreal]=imread([imgRealSegFolder imgRealSegList(rr).name]);
        realClsIdx = setdiff(unique(realSegImg),[0 255]);
        for rii = 1 : length(realClsIdx)
            if realClsIdx(rii) ~= targetClassLabel
                %disp(['Instance is not ' targetClassName ', continue!']);
                continue;
            end;
            predClassMaskNum = predClassMaskNum + 1;
            realInstMask = double(realSegImg == realClsIdx(rii));
            [r c v] = find(realInstMask);
            if length(find(realInstMask)) < minMaskPixThrsh
                disp('Mask Too Small, Dump!'); continue; end;
            bbox = [max(min(c)-10,1)  max(min(r)-10,1) min(max(c)+10,imgW) min(max(r)+10,imgH)];
            %%%%%%%%%%%%%%%%%%%%%
            realInstMask = realInstMask(bbox(2):bbox(4),bbox(1):bbox(3)); % bbox: [x1 y1 x2 y2];[y1:y2 x1:x2]
            orgInstImg = orgImage(bbox(2):bbox(4),bbox(1):bbox(3),:);
            realInstGTMask = targetClassGTMask(bbox(2):bbox(4),bbox(1):bbox(3),:);
            %%%%%%%%%%%%%%%%%%%%%
            realInstMask = imresize(realInstMask,refMaskSize);
            orgInstImg = imresize(orgInstImg,refMaskSize);
            realInstGTMask = imresize(realInstGTMask,refMaskSize);
            %%%%%%%%%%%%%%%%%%%%%
            realInstMask = padarray(realInstMask,[padr padr]);
            orgInstImg = padarray(orgInstImg,[padr padr],'symmetric');%'symmetric');
            realInstGTMask = padarray(realInstGTMask,[padr padr]);
            %%%%%%%%%%%%%%%%%%%%%
            %% sample points for patches
            [xre,yre,tre]=bdry_extract_3(realInstMask);
            nsamp2=length(xre);
            if nsamp2>=nsampTest
                [xre,yre,tre]=get_samples_1(xre,yre,tre,nsampTest);
            end
            xre = round(xre); yre = round(yre);
            %%% No need to padd: points are sampled on padded masks
            %xre = xre + padr; yre = yre + padr;
            %negData.points = [negData.points; realPoints];
            %% Sample negative patches along false positive boundaries
            dy = dy; dx = 0;
            batchData = zeros(subwinSize,subwinSize,4,nsampTest);
            batchPatchMasks = {};
            batchGTMasks = {};
            batchOvlpGT = [];
            for ii = 1 : nsampTest
                %$% No Translation
                testPatchImg = orgInstImg((yre(ii)-winR):(yre(ii)+winR), (xre(ii)-winR):(xre(ii)+winR),:);
                testPatchMask = realInstMask( (yre(ii)-winR):(yre(ii)+winR), (xre(ii)-winR):(xre(ii)+winR));
                testPatchMask = double(testPatchMask>0);
                testPatchGTMask = realInstGTMask( (yre(ii)-winR):(yre(ii)+winR), (xre(ii)-winR):(xre(ii)+winR));
                ovlpGT = fbIntUni(testPatchMask, testPatchGTMask); %length(find(testPatchMask .* testPatchGTMask)) / length( find(testPatchMask + testPatchGTMask));
                batchPatchMasks{end+1} = testPatchMask;
                batchGTMasks{end+1} = testPatchGTMask;
                batchOvlpGT(end+1) = ovlpGT;
                %testPatchMask = permute(testPatchMask, [2 1 3 4]);
                testData = zeros([size(testPatchMask) 4]);
                testData(:,:,1:3) = testPatchImg;
                testData(:,:,4) = testPatchMask;
                batchData(:,:,:,ii) = testData;
                curPatchNum = curPatchNum + 1;
            end;
            %% 3/3/2015: Should permute after read from HDF5! (Since the HDF Train/Test data did NOT permute([2 1 3 4]), therefore the test data will be transposedly used in Caffe
            %  Therefore batchData needs to be permute in Matlab before
            %  feeding into Caffe (while the data read from the
            %  unpermuted test_HDF.h5 doesn't
            % batchData = permute(batchData, [2 1 3 4]);
            %%
            inputData = {single(batchData)};
            tic;
            softmaxOut = caffe('forward', inputData); toc;
            outProb = softmaxOut{1};
            outProb = outProb(:,:,2,:);
            outProb = permute(outProb, [4 1 2 3]);
            disp(['Processed ' num2str(curPatchNum) ' Data']);
            %% visualize
            posOvlpThrsh = 0.8; negOvlpThrsh = 0.3; labelThrsh = 0.5;
            posIdx = find(batchOvlpGT >= posOvlpThrsh); 
            negIdx = find(batchOvlpGT < negOvlpThrsh);
            gtLabelsAll = (batchOvlpGT >= labelThrsh)';
            gtLabelsHard = -1 * ones(size(batchOvlpGT))';
            gtLabelsHard(posIdx) = 1; gtLabelsHard(negIdx) = 0;
            outLabels = (outProb >= labelThrsh);
            allOutLabels = [allOutLabels; outLabels;];
            allGTLabels = [allGTLabels; gtLabelsHard];
            accAll = length(find(outLabels == gtLabelsAll)) / length(gtLabelsAll);
            accHard = length(find(outLabels == gtLabelsHard)) / length(find(gtLabelsHard~=-1));
            if ~isnan(accHard)
                accuracyHard(end+1) = accHard; end;
            accuracyAll(end+1) = accAll;
            title4 = sprintf('AccHard: %.2f%%, AccAll: %.2f%%',accHard*100, accAll*100);
            disp(title4);
            visualFlag = 1;
            saveVisFlag = 1;
            if visualFlag
                titleStrs = {'','','',title4};
                if saveVisFlag
                    if ~isdir(saveVisImgFolder) mkdir(saveVisImgFolder); end;
                    saveVisName = [saveVisImgFolder imgRealSegList(rr).name];
                    visvar = {'saveImgName' saveVisName};
                else visvar = {}; end;
                visBoundaryCls(orgInstImg, realInstMask, realInstGTMask, batchPatchMasks, ...
                    [xre yre],outProb,batchOvlpGT,titleStrs, visvar);
            end;
        end; % rii
    end;  % rr
end; % mm
%%
meanAccHard = mean(accuracyHard);
meanAccAll = mean(accuarcyAll);
[confMat order] = confusionmat(allOutLabels, allGTLabels);
posNum = sum(confMat(1,:));
negNum = sum(confMat(2,:));
fpr = confMat(2,1) / posNum;
fnr = confMat(1,2) / negNum;
disp(['Average Accuracy = ' num2str(meanAcc*100) '%']);
disp(['Total False Positive Percentage: ' num2str(confMat(2,1)*100) '%']);
disp(['Total False Negative Percentage: ' num2str(confMat(1,2)*100) '%']);
%%
[p saveFileName1 f] = fileparts(model_def_file);
[p saveFileName2 f] = fileparts(model_file);
saveFileName = [saveFileName1 '_' saveFileName2];
save([saveResFolder saveFileName '_confMat.mat'],'allGTLabels','allOutLabels','confMat','meanAccHard','meanAccAll');

return;


%%
function [resBbox] = getResBbox(img, newSize,pixValThrsh)
[r c v] = find(img);
resBbox = img(min(r):max(r),min(c):max(c));
resBbox = imresize(resBbox, newSize);
resBbox = double(resBbox > pixValThrsh);