function [ res ] = EvaluateBatchData( img_raw, img, suppix, sample, psize, start_id, batch_size )
%GETBATCHDATA Summary of this function goes here
%   Detailed explanation goes here
[H, W,~]=size(img);
half_size=int32(psize/2);
n=length(sample);

nsampleTest=min(batch_size,n-start_id+1);
batchData = zeros(psize,psize,4,0);

patch_id=1;
patch_sample_id=zeros(0,1);
for i=start_id:start_id+nsampleTest-1
    h=sample(i,1)-half_size;
    w=sample(i,2)-half_size;
    if h<1 || w<1 || h+psize-1 > H || w+psize-1 > W
        continue;
    end
    testPatchImg = img(h:h+psize-1,w:w+psize-1,:);
    cur_sup=suppix(h:h+psize-1,w:w+psize-1);
    
    testPatchImg_raw=img_raw(h:h+psize-1,w:w+psize-1,:);
    cur_sup_cluster = cluster_patch(testPatchImg_raw, cur_sup,10);
    
    cluster_id=unique(cur_sup_cluster);
    cluster_num=length(cluster_id);
    
    
    for j=1:power(2,cluster_num)
        str=dec2bin(j-1,cluster_num);
        testPatchMask=zeros(psize,psize);
        for k=1:cluster_num
           testPatchMask(cur_sup_cluster==k)=str2num(str(k));
        end
        flag1=is_center_on_boundary(testPatchMask,half_size);
        
        if flag1==false
            continue;
        end
        flag2=is_connected(testPatchMask);
        if flag2==false
            continue;
        end
        fprintf('cluster num: %d\n',cluster_num);
        testData = zeros([size(testPatchMask) 4]);
        testData(:,:,1:3)=testPatchImg;
        testData(:,:,4)=testPatchMask;
        batchData(:,:,:,patch_id)=testData;
        patch_sample_id(patch_id,1)=i-start_id+1;
        patch_id=patch_id+1;
    end    
end

res=zeros(nsampleTest,1);
if patch_id==1
    return;
end

fixed_dim=40;
outProb_res=zeros(0,1);
batch_length=length(patch_sample_id);
while true
    batchData2=zeros(psize,psize,4,0);
    if batch_length<fixed_dim
        for i=batch_length+1:fixed_dim
            testData(:,:,1:3)=zeros(psize,psize,3);
            testData(:,:,4)=zeros(psize,psize);
            batchData(:,:,:,i)=testData;
        end
    elseif batch_length>fixed_dim
        batchData2=batchData(:,:,:,fixed_dim+1:batch_length);
        batchData=batchData(:,:,:,1:fixed_dim);
        batch_length=fixed_dim;
    end
    inputData = {single(batchData)};
    softmaxOut = caffe('forward', inputData); 
    outProb = softmaxOut{1};
    outProb = outProb(:,:,2,:);
    outProb = permute(outProb, [4 1 2 3]);
    outProb_res=[outProb_res;outProb(1:batch_length,1)];
    batchData=batchData2;
    [~,~,~,batch_length]=size(batchData);
    if batch_length==0
        break;
    end
end

for i=1:length(patch_sample_id)
    if outProb_res(i)>res(patch_sample_id(i))
        res(patch_sample_id(i))=outProb_res(i);
    end
end

end

function [flag] = is_center_on_boundary(patch,half_size)
    flag=false;
    for k=half_size-2:half_size+2
        for l=half_size-2:half_size+2
            if is_boundary(patch,k,l)
                flag=true;
            end
        end
    end
end

function [flag] = is_connected(patch)
    flag=true;
    t=bwconncomp(patch);
    if t.NumObjects>1
        flag=false;
    end
    t2=bwconncomp(1-patch);
    if t2.NumObjects>1
        flag=false;
    end  
end
